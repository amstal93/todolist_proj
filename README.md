# TodoReminder -- Todo Notes App API written in python using Django and DRF 

My python learning project 

## Features:
* CRUD operations on notes
* on-email notifications using celery
* JWT-based Authorization
* user groups for notes
* photo uploading to note 
* tests
* docker-compose file


Run by typing
```
docker-compose up
```

    