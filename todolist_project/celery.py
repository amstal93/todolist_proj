from __future__ import absolute_import, unicode_literals
import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'todolist_project.settings')

app = Celery('todolist_project',  backend='rpc://', broker='pyamqp://rabbitmq:rabbitmq@rabbitmq:5672/')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


app.conf.beat_schedule = {
    'notify-before-hour': {
        'task': 'todo.tasks.notify_before_hour',
        'schedule': 60.0,
    },
    'check-outdated-todos': {
        'task': 'todo.tasks.notify_users_with_outdated_todos',
        'schedule': 60.0,
    },
}


app.conf.timezone = 'UTC'
