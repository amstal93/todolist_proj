from rest_framework import serializers

from .models import Group, UserGroupRole


class GroupSerializer(serializers.ModelSerializer):
    users_count = serializers.IntegerField(read_only=True, source='num_users')

    def create(self, validated_data):
        user = validated_data.get('creator')
        group = Group.objects.create(**validated_data)
        UserGroupRole.objects.create(
             user=user,
             group=group,
             is_admin=True,
        )
        return group

    class Meta:
        model = Group
        fields = ('id', 'title', 'users_count', 'creator')
        read_only_fields = ('creator',)


class UsersGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserGroupRole
        fields = ('user', 'is_admin')
        read_only_fields = ('user',)
