from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register(r'', views.GroupViewset, base_name='groups-viewset')


urlpatterns = router.urls
