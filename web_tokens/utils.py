import jwt
import datetime

try:
    from .settings import JWT_PAYLOAD_FIELDS

    if not isinstance(JWT_PAYLOAD_FIELDS, (tuple, list)):
        msg = "Not a valid type of JWT_PAYLOAD_FIELDS in app settings"
        raise ImportError(msg)

except ImportError:
    msg = "JWT_PAYLOAD_FIELDS not found in app settings"
    raise ImportError(msg)

from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.auth.models import User

from rest_framework import exceptions


def generate_token_pair(user):
    """
    Generates token pair consisting of refresh_token and access_token
    """
    if not isinstance(user, User):
        msg = "Not valid type of user"
        raise TypeError(msg)

    user_id = user.id
    access_token_exp = datetime.datetime.utcnow() + settings.JWT['JWT_ACCESS_EXPIRATION_DELTA']
    refresh_token_exp = datetime.datetime.utcnow() + settings.JWT['JWT_REFRESH_EXPIRATION_DELTA']

    access_token_payload = {
        'user_id': user_id,
        'exp': access_token_exp
    }

    refresh_token_payload = {
        'user_id': user_id,
        'exp': refresh_token_exp
    }

    access_token = jwt.encode(
            access_token_payload,
            key=settings.SECRET_KEY,
            algorithm='HS256').decode('utf-8')

    refresh_token = jwt.encode(
            refresh_token_payload,
            key=settings.SECRET_KEY,
            algorithm='HS256').decode('utf-8')

    return {
        'access_token': access_token,
        'refresh_token': refresh_token
    }


def get_token_payload(token):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY)
    except jwt.exceptions.InvalidSignatureError:
        msg = _("Signature verification failed")
        raise exceptions.PermissionDenied(msg)
    except jwt.exceptions.ExpiredSignatureError:
        msg = _("Signature has expired!")
        raise exceptions.PermissionDenied(msg)
    except jwt.InvalidTokenError:
        msg = _("Invalid token error")
        raise exceptions.NotAuthenticated(msg)

    return payload


def validate_payload(payload):
    """
    Checks JWT payload for required fields
    """

    # Checking for required fields in payload
    for field in payload.keys():
        if field not in JWT_PAYLOAD_FIELDS:
            return None

    return True
