from .models import Refresher
from .utils import generate_token_pair, validate_payload, get_token_payload

from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from rest_framework.views import APIView
from rest_framework.exceptions import ParseError
from rest_framework.response import Response


class ObtainTokenPair(APIView):
    """
    API View that returns generated JWT token pair. Refresh token obtained by a user
    is stored in the database in 'Refresher' model as refresh_token field.
    """
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        if not username or not password:
            msg = _("username and password required")
            raise ParseError(detail=msg)

        user = self.check_user(username=username, password=password)

        token_pair = generate_token_pair(user=user)
        refresh_token = token_pair.get('refresh_token', None)

        # update or create
        Refresher.objects.update_or_create(user=user, refresh_token=refresh_token)

        return Response(data=token_pair)

    def check_user(self, username, password):
        msg = _("Invalid username or password")
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ParseError(detail=msg)
        is_correct_password = user.check_password(password)
        if not is_correct_password:
            raise ParseError(detail=msg)
        return user


class RefreshTokenPair(APIView):
    """
    API View that returns a new token pair if user's refresh_token has not expired.
    It updates `Refresher` model in that case and deletes the row in other case.
    """
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        refresh_token = request.data.get('refresh_token', None)

        if not refresh_token:
            msg = _("refresh_token is required")
            raise ParseError(msg)

        payload = get_token_payload(refresh_token)
        if not validate_payload(payload=payload):
            msg = _("refresh_token wrong format")
            raise ParseError(msg)

        payload = get_token_payload(token=refresh_token)

        user_id = payload.get('user_id', None)
        user = User.objects.get(pk=user_id)

        if not user.is_active:
            msg = _("User is not activated")
            raise ParseError(msg)

        try:
            refresher = user.refresher_set.get(refresh_token=refresh_token)
            token_pair = generate_token_pair(user=refresher.user)
            refresher.refresh_token = token_pair.get('refresh_token', None)
            refresher.save()
        except Refresher.DoesNotExist:
            msg = _("Non existing refresh_token")
            raise ParseError(msg)

        return Response(data=token_pair)
