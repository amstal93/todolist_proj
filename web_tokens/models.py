from django.db import models
from django.contrib.auth.models import User


class Refresher(models.Model):
    refresh_token = models.CharField(max_length=512)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
