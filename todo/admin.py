from .models import Todo, TodoList

from django.contrib import admin

admin.site.register(Todo)
admin.site.register(TodoList)
