from django_filters import rest_framework as filters
from .models import TodoList


class TodoListFilter(filters.FilterSet):
    title = filters.CharFilter(lookup_expr='contains')

    class Meta:
        model = TodoList
        fields = ['title', 'completed']
