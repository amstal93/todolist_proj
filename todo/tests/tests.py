import mimetypes
from io import BytesIO
from datetime import datetime, timedelta

from todo.models import Todo, TodoList
from todo.tasks import (
    notify_users_with_outdated_todos,
    notify_before_hour
)
from todo.tasks import app as celery_app

from django.urls import reverse
from django.core import mail
from django.contrib.auth.models import User
from django.conf import settings

from rest_framework.test import APITestCase
from rest_framework import status


class TodoTest(APITestCase):

    def setUp(self):
        self.username = "tester"
        self.password = "testing1"
        self.email = "tester@testing.com"

        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
            email=self.email,
        )

        self.testing_data = {
            'title': 'Test',
            'text': 'Testing',
        }
        self.client.force_authenticate(user=self.user)
        self.access_token = self.get_access_token()
        self.access_token_auth = f"Authorization: {self.access_token}"

        self.test_todo_list_obj = TodoList.objects.create(
            title='Testing Title',
            user=self.user
        )
        self.test_todo_obj = Todo.objects.create(
            title='Testing Title',
            text='Testing text',
            user=self.user,
            todo_list=self.test_todo_list_obj
        )

    ################################
    #                              #
    # REGISTRATION, RESPONSE FORMAT#
    #       AND TOKENS TESTS       #
    #                              #
    ################################

    # get access_token
    def get_access_token(self):
        auth = {"username": self.username, "password": self.password}
        url = reverse('web_tokens:obtain-token-pair')
        response = self.client.post(url, data=auth)
        self.assertIsNotNone(response.data.get('access_token'))
        access_token = response.get('access_token')
        return access_token

    # Testing registration
    def test_register_user(self):
        username = self.username + "_test_register"
        email = "_test_register" + self.email
        data = {
            "username": username,
            "password": self.password,
            "email": email
        }
        url = reverse("register")
        response = self.client.post(url, data=data)
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            response.data
        )

    # Testing response format
    def test_response_format(self, ):
        response_set = {
            'id', 'title', 'text',
            'to_be_done_date', 'created_at',
            'is_done', 'todo_list', 'user',
            'failed', 'image',
        }
        url = reverse('todo:todo-viewset-detail', args=[self.test_todo_obj.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(set(response.data.keys()), response_set, response.data)

    ################################
    #                              #
    #       TODOLISTS TESTS        #
    #                              #
    ################################

    # Bug test
    def test_create_todolist_user_not_null(self):
        access_token_auth = f"Authorization: {self.access_token}"
        url = reverse('todo:todolists-list')
        testing_data = {
            'title': 'Testing List',
        }
        response = self.client.post(
            url,
            data=testing_data,
            HTTP_AUTHORIZATION=access_token_auth
        )
        user_id = response.data.get("user", None)
        self.assertEqual(user_id, self.user.id, response.data)

    # POST create TodoList
    def test_create_todolist(self):
        access_token_auth = f"Authorization: {self.access_token}"
        url = reverse('todo:todolists-list')
        testing_data = {
            'title': 'Testing List',
        }
        response = self.client.post(
            url, data=testing_data,
            HTTP_AUTHORIZATION=access_token_auth
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # Get list of TodoLists
    def test_get_todolist_list(self):
        url = reverse('todo:todolists-list')
        response = self.client.get(
            url,
            HTTP_AUTHORIZATION=self.access_token_auth
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    # Get TodoList detail
    def test_get_todolist_detail(self):
        url = reverse('todo:todolist-detail', args=[self.test_todo_list_obj.id])
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token_auth)
        todolist_id = response.data.get('id', None)
        self.assertIsNotNone(todolist_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(todolist_id, self.test_todo_list_obj.id)

    # PATCH TodoList test
    def test_patch_todolist_detail(self):
        patched_data = {'title': 'patched_title'}
        url = reverse('todo:todolist-detail', args=[self.test_todo_list_obj.id])
        response = self.client.patch(
            url, data=patched_data,
            HTTP_AUTHORIZATION=self.access_token_auth
        )
        title = response.data.get('title', None)
        self.assertIsNotNone(title)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(title, patched_data.get('title'))

    # PUT TodoList test
    def test_put_todolist_detail(self):
        put_data = {'title': 'put_title', 'user': self.user.id}
        url = reverse('todo:todolist-detail', args=[self.test_todo_list_obj.id])
        response = self.client.put(
            url,
            data=put_data,
            HTTP_AUTHORIZATION=self.access_token_auth
        )
        title = response.data.get('title', None)
        self.assertIsNotNone(title)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(title, put_data.get('title'))

    # DELETE TodoList test
    def test_delete_todolist(self):
        url = reverse('todo:todolist-detail', args=[self.test_todo_list_obj.id])
        response = self.client.delete(
            url,
            HTTP_AUTHORIZATION=self.access_token_auth
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    ################################
    #                              #
    #         TODOS TESTS          #
    #                              #
    ################################

    # GET Todos
    def test_get_todos_api_list(self):
        url = reverse('todo:todo-viewset-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token_auth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    # GET Todo detailed
    def test_todos_api_detail(self):
        url = reverse('todo:todo-viewset-detail', args=[self.test_todo_obj.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('title'), self.test_todo_obj.title)
        self.assertEqual(response.data.get('todo_list'), self.test_todo_list_obj.id)
        # self.assert_response(response, status.HTTP_200_OK, **data)

    # POST create todo test
    def test_todos_api_create(self):
        data = {
            'title': 'Testing',
            'text': 'Create simple test',
            'todo_list': self.test_todo_list_obj.id
        }
        url = reverse('todo:todo-viewset-list')
        response = self.client.post(url, data=data)
        self.assertIsNotNone(response.data.get('id'), response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('title'), data.get('title'))
        self.assertEqual(response.data.get('text'), data.get('text'))
        self.assertEqual(response.data.get('todo_list'), data.get('todo_list'))

    # PATCH todo test
    def test_todos_api_patch(self):
        data = {'title': 'Patched title'}
        url = reverse('todo:todo-viewset-detail', args=[self.test_todo_obj.id])
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('title'), data.get('title'))

    # PUT todo test
    def test_todos_api_put(self):
        to_be_done_date = datetime.now().isoformat()
        url = reverse('todo:todo-viewset-detail', args=[self.test_todo_obj.id])
        data = {'title': 'Put test', 'text': 'Put test', 'is_done': True,
                'to_be_done_date': to_be_done_date, 'todo_list': self.test_todo_list_obj.id}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data.get('title'), data.get('title'))

    # DELETE todo test
    def test_todos_api_delete(self):
        url = reverse('todo:todo-viewset-detail', args=[self.test_todo_obj.id])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)


class TodoTasksTest(APITestCase):

    def setUp(self):
        self.username = "tester"
        self.password = "testing1"
        self.email = "tester@testing.com"

        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
            email=self.email,
        )

        self.test_todo_list = TodoList.objects.create(title='Testing Title', user=self.user)
        self.outdated_todo = Todo.objects.create(
            title='Outdated todo',
            text='Text for testing',
            failed=False,
            user=self.user,
            todo_list=self.test_todo_list,
            to_be_done_date=datetime.now() - timedelta(days=1)
        )
        self.before_hour_todo = Todo.objects.create(
            title='Title for testing',
            text='Text for testing',
            failed=False,
            user=self.user,
            todo_list=self.test_todo_list,
            to_be_done_date=datetime.now() + timedelta(hours=1)
        )
        celery_app.conf.task_always_eager = True

    def tearDown(self):
        celery_app.conf.task_always_eager = False

    def test_notify_users_with_outdated_todos(self):
        subject = f'You failed todo: {self.outdated_todo.title}'

        notify_users_with_outdated_todos()
        self.outdated_todo.refresh_from_db()

        self.assertEqual(self.outdated_todo.failed, True)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, subject)

    def test_notify_before_hour(self):
        subject = f"1 Hour left before your todo: {self.before_hour_todo.title}"
        notify_before_hour()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, subject)
        now = datetime.now().strftime("%Y-%m-%d %H:%M")
        before_hour_todo_date = (
                self.before_hour_todo.to_be_done_date - timedelta(hours=1))\
            .strftime("%Y-%m-%d %H:%M")
        self.assertEqual(before_hour_todo_date, now)


class TestTodoImageUpload(APITestCase):

    def setUp(self):
        self.username = "tester"
        self.password = "testing1"
        self.email = "tester@testing.com"

        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
            email=self.email,
        )

        self.test_todo_list = TodoList.objects.create(title='Testing Title', user=self.user)
        self.todo = Todo.objects.create(
            title='Outdated todo',
            text='Text for testing',
            failed=False,
            user=self.user,
            todo_list=self.test_todo_list,
        )

        self.client.force_authenticate(user=self.user)
        self.access_token = self._get_access_token()
        self.access_token_auth = f"Authorization: {self.access_token}"
        self.url = reverse('todo:todo-viewset-todo-image-view',  args=[self.todo.id])
        self.img_local_path = "/todo/tests/testing_images/test.jpg"
        self.img_abs_path = settings.BASE_DIR + self.img_local_path

    def test_only_image_accepted(self):
        binary_crap_data = BytesIO(b'28605495YHlCJfMKpRPGyAw')
        data = {'image': binary_crap_data}
        response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.access_token_auth, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_image_none(self):
        data = {'image': ''}
        response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.access_token_auth, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_image_field_required(self):
        data = {'my_field': ''}
        response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.access_token_auth, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_upload_todo_image(self):
        image = open(self.img_abs_path, 'rb')
        data = {'image': image}
        response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.access_token_auth, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_todo_image(self):
        self.test_upload_todo_image()
        response = self.client.delete(self.url, auth=self.access_token_auth)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_get_todo_image(self):
        self.test_upload_todo_image()
        response = self.client.get(self.url, auth=self.access_token_auth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_content_type = response.get('Content-Type')
        content_type = mimetypes.guess_type(str(self.img_abs_path))[0]
        self.assertEqual(response_content_type, content_type, response._content_type_for_repr)

    def _get_access_token(self):
        auth = {"username": self.username, "password": self.password}
        url = reverse('web_tokens:obtain-token-pair')
        response = self.client.post(url, data=auth)
        self.assertIsNotNone(response.data.get('access_token'))
        access_token = response.get('access_token')
        return access_token
